--[[--------------------------------------------------------------------------------------------------------------------

  Application Name:

    LMS4000 Demo Case

  Version:

    V0.0.1
  
  Author:

    SICK AG, Silas Gschwender

  Summary:

    This app creates 3D pointclouds by adding up scans using an ecoder. Inductive switches which are placed in
    the demo case check the end position of the rotation.

    All settings can be set with SOPAS. However, the app overwrites the settings for the mean filter and the encoder.
      -- Changing the output setting format
        -- checking / unchecking Encoder ticks has no effect to the app
        -- checking / unchecking angular channel has no effect; it't not used.
        -- RSSI with 'digits' is preferd to get a good intesity visalization
        -- make sure a valid SD card is inserted, otherwise the recorded pointclouds are wiped after a power cycle

----------------------------------------------------------------------------------------------------------------------]]



------------------------------------------------------------------------------------------------------------------------
-- Setup
------------------------------------------------------------------------------------------------------------------------
--set luacheck global variables
--luacheck: globals PortDI1 PortDI2 ledSysReady

local scannerIP = '192.168.136.1' -- IP adress of LMS4000
local recTime = 30 --secondes
local recTimeBuf = 0
local meanFilterSize = 10
local appState = 'Move the LiDAR to the lower position to enable the recording.'
local appStateOld = ''
local scanQueueHandle = nil
local scanCount = 0
local encoderIncrements = 0
local encoderIncrementsOffset = 0
local lidar = {
  firmwareVersion = '',
  orderNumber = '',
  serialNumber = '',
  deviceType = '',
  powerOnCounter = '',
  ipAdress = scannerIP
}
local sim = {
  firmwareVersion = '',
  orderNumber = '1098148',
  serialNumber = '',
  deviceType = '',
  powerOnCounter = '',
  ipAdress = scannerIP
}
local encoder = {orderNumber = '1070596', deviceType = 'DBS36E-S3EP01024'}
local inductiveSwitch = {orderNumber = '1075584', deviceType = 'IMB08-04NDSVT0K'}
local thumbnailIndex = 1
local thumbnailIndexOld = 1
local numberOfStoredFiles = 0
local numberOfStoredFilesOld = 0
local archivePath = 'sdcard/0/sick/records/' -- archivePath to save the recorded data in
local sampleSource = 'resources/pcd/sample.pcd' -- samplePath to have a good sample
local angle = 0 --scanner angle
local pointClouds = {}

-- serve events
Script.serveEvent('DemoCase_LMS4000.updateState', 'updateState')
Script.serveEvent('DemoCase_LMS4000.updateFileCount', 'updateFileCount')
Script.serveEvent('DemoCase_LMS4000.OnNewScan', 'handleOnNewScan')
Script.serveEvent('DemoCase_LMS4000.printDescription', 'printDescription')
Script.serveEvent('DemoCase_LMS4000.printRecordingProcedure','printRecordingProcedure')
Script.serveEvent('DemoCase_LMS4000.printDownloadingProcedure','printDownloadingProcedure')
Script.serveEvent('DemoCase_LMS4000.printChangingParameterProcedure','printChangingParameterProcedure')
Script.serveEvent('DemoCase_LMS4000.printGoodToKnow', 'printGoodToKnow')
Script.serveEvent('DemoCase_LMS4000.printLMS', 'printLMS')
Script.serveEvent('DemoCase_LMS4000.deactivateApplySettings', 'deactivateApplySettings')
Script.serveEvent('DemoCase_LMS4000.deactivateReturnLiveData', 'deactivateReturnLiveData')
Script.serveEvent('DemoCase_LMS4000.deactivateReconnect', 'deactivateReconnect')
Script.serveEvent('DemoCase_LMS4000.deactivateDeleteAllFiles', 'deactivateDeleteAllFiles')
Script.serveEvent('DemoCase_LMS4000.deactivateLoadPointCloud', 'deactivateLoadPointCloud')
Script.serveEvent('DemoCase_LMS4000.printThumbnailFileName', 'printThumbnailFileName')
Script.serveEvent('DemoCase_LMS4000.printThumbnailPoints', 'printThumbnailPoints')
Script.serveEvent('DemoCase_LMS4000.printThumbnailLength', 'printThumbnailLength')
Script.serveEvent('DemoCase_LMS4000.printThumbnailWidth', 'printThumbnailWidth')
Script.serveEvent('DemoCase_LMS4000.printThumbnailHeight', 'printThumbnailHeight')
Script.serveEvent('DemoCase_LMS4000.printThumbnailMemSize', 'printThumbnailMemSize')
Script.serveEvent('DemoCase_LMS4000.setDownloadFilePath', 'setDownloadFilePath')
Script.serveEvent('DemoCase_LMS4000.setDownloadFileName', 'setDownloadFileName')
Script.serveEvent('DemoCase_LMS4000.printSIM', 'printSIM')
Script.serveEvent('DemoCase_LMS4000.printThumbnailCharacteristics', 'printThumbnailCharacteristics')
Script.serveEvent('DemoCase_LMS4000.printEncoder', 'printEncoder')
Script.serveEvent('DemoCase_LMS4000.printSwitches', 'printSwitches')

--scanner
local scanprovider = nil --Scan.Provider.RemoteScanner.create()

--IOs
--power for inductive end switches
PowerHandleS1 = Connector.Power.create("S1")

PortDI1 = Connector.DigitalIn.create('S1DI1') -- need to global
PortDI1:setLogic('ACTIVE_HIGH')
assert(PortDI1, 'PortDI1 could not be created.')

PortDI2 = Connector.DigitalIn.create('S1DI2') -- need to global
PortDI2:setLogic('ACTIVE_HIGH')
assert(PortDI2, 'PortDI2 could not be created.')

--SIM
sim.deviceType = Engine.getTypeName()
sim.firmwareVersion = Engine.getFirmwareVersion()
sim.serialNumber = Engine.getSerialNumber()
sim.ipAdress = Ethernet.getInterfaceConfig("ETH1")
ledSysReady = LED.create("LED_SYS_RDY")
ledResult = LED.create("LED_RESULT")

--GUI
local trafo = Scan.Transform.create()
trafo:setPosition(65, 0, 0) --65: add half of scanner size as the distance value count from the front screen

local scanViewer = View.create('scanViewer')
assert(scanViewer, 'Scan viewer could not be created.')

local thumbnailViewer = View.create('Thumbnail')
assert(scanViewer, 'Thumbnail viewer could not be created.')

local pointCloudCollector = PointCloud.Collector.create()
pointCloudCollector:setInitialPointCount(1000000)

local pointCloudDeco = View.PointCloudDecoration.create()
pointCloudDeco:setIntensityColormap(2)
pointCloudDeco:setIntensityRange(0, 200) --intensity colours do not match with SOPAS as it's a different platform
pointCloudDeco:setPointSize(10)

-- Sensor visualisation
local lmsDecoBlue = View.ShapeDecoration.create()
lmsDecoBlue:setFillColor(0, 137, 182) --scanner color blue
lmsDecoBlue:setLineColor(0, 0, 0)

local lmsDecoBlack = View.ShapeDecoration.create()
lmsDecoBlack:setFillColor(0, 0, 0) --scanner color black
lmsDecoBlack:setLineColor(0, 0, 0)

local laserDeco = View.ShapeDecoration.create()
laserDeco:setFillColor(204, 0, 0)
laserDeco:setLineColor(204, 0, 0)

--timer to check a maximum of collected data on a time base
local timer1 = Timer.create()
timer1:setExpirationTime(100)
timer1:stop()
local count100ms = 0

------------------------------------------------------------------------------------------------------------------------
-- Local functions
------------------------------------------------------------------------------------------------------------------------

--@setupLMS()
local function setupLMS()
  -- Setup includes:
  --lasercontrol: free running
  --mean filter: 10
  local success

  -- Create and configure connection for SOPAS parametrization
  local tcpipClient = TCPIPClient.create()
  tcpipClient:setIPAddress(scannerIP)
  tcpipClient:setPort(2112)
  tcpipClient:connect(5000)
  if tcpipClient:isConnected() then
    -- Create, configure and open the command client
    local commandClient = Command.Client.create()
    --the sopas file includes the variable structur and is therefor neeed
    commandClient:setDescriptionFile('resources/sopas/LMS41xxx.sopas')
    commandClient:setDeviceSelector('', 'LMS41xxx')
    commandClient:setProtocol('COLA_A')
    commandClient:setConnection(tcpipClient)

    if commandClient:open() then -- '.sopas'-file is loaded
      --LMS connected

      -- Login to device
      local SetAccessModeNode = commandClient:createNode('SetAccessMode')
      SetAccessModeNode:set(4, 'NewMode') -- userlevel 'Service'
      SetAccessModeNode:set(0x81BE23AA, 'Password') -- encoded password
      local SetAccessModeSuccess,
        SetAccessModeResult =
        commandClient:invoke('SetAccessMode', SetAccessModeNode)
      assert(SetAccessModeSuccess)
      assert(SetAccessModeResult:get('success'), 'Login failed') -- failed to get access

      --read device type
      local DItypeNode = commandClient:read('DItype')
      lidar.deviceType = DItypeNode:get()

      --read order number
      local serialOrderNumberNode = commandClient:read('OrderNumber')
      lidar.orderNumber = serialOrderNumberNode:get()

      --read device firmware version
      local firmwareVersionNode = commandClient:read('FirmwareVersion')
      lidar.firmwareVersion = firmwareVersionNode:get()

      --read serial number
      local serialNumberNode = commandClient:read('SerialNumber')
      lidar.serialNumber = serialNumberNode:get()

      -- Set encoder setting to "phase" (first to "no encoder" than tp "phase" resets the value)
      local LICencsetNode = commandClient:read('LICencset')
      local phaseSetting = LICencsetNode:getNode()
      phaseSetting:set(0) --0:"no enocder" -- resets the encoder value
      LICencsetNode:setNode(phaseSetting)
      assert(
        commandClient:write('LICencset', LICencsetNode),
        'Setting encoder filter failed'
      )

      LICencsetNode = commandClient:read('LICencset')
      phaseSetting = LICencsetNode:getNode()
      phaseSetting:set(2) --2:"direction mode phase"
      LICencsetNode:setNode(phaseSetting)
      assert(
        commandClient:write('LICencset', LICencsetNode),
        'Setting encoder filter failed'
      )

      -- Set laser control to "free running"
      local IOlascNode = commandClient:read('IOlasc')
      local IOlascNodeSetting = IOlascNode:getNode('TriggerSource')
      IOlascNodeSetting:set(0) --0: "free running"
      IOlascNode:setNode(IOlascNodeSetting, 'TriggerSource')
      assert(
        commandClient:write('IOlasc', IOlascNode),
        'Setting laser control filter failed'
      )

      -- Set mean filter
      local LFPmeanfilterNode = commandClient:read('LFPmeanfilter')
      local bEnableNode = LFPmeanfilterNode:getNode('bEnable')
      bEnableNode:set(true)
      LFPmeanfilterNode:setNode(bEnableNode, 'bEnable')
      local meanFilterDepthNode = LFPmeanfilterNode:getNode('uiScanNumber')
      meanFilterDepthNode:set(meanFilterSize) --mean filter size
      LFPmeanfilterNode:setNode(meanFilterDepthNode, 'uiScanNumber')
      assert(
        commandClient:write('LFPmeanfilter', LFPmeanfilterNode),
        'Setting mean filter failed'
      )

      -- Run
      local RunSuccess,
        RunResult = commandClient:invoke('Run')
      assert(RunSuccess)
      assert(RunResult:get('success'), 'Run method not executed')

      success = true
    else
      print('[ERROR]:LMS not connected - check cabeling and SOPAS ET settings')
      Script.notifyEvent(
        'updateState',
        'LMS not connected - check cabeling and SOPAS ET settings'
      )
      success = false
    end

    commandClient:close()
    tcpipClient:disconnect()
  else
    success = false
  end
  return success
end

--@resetEncoderValue (first to "no encoder" than tp "phase" resets the value)
local function resetEncoderValue()
  local tcpipClient = TCPIPClient.create()
  tcpipClient:setIPAddress(scannerIP)
  tcpipClient:setPort(2112)
  tcpipClient:connect(5000)
  if tcpipClient:isConnected() then
    -- Create, configure and open the command client
    local commandClient = Command.Client.create()
    --the sopas file includes the variable structur and is therefor neeed
    commandClient:setDescriptionFile('resources/sopas/LMS41xxx.sopas')
    commandClient:setDeviceSelector('', 'LMS41xxx')
    commandClient:setProtocol('COLA_A')
    commandClient:setConnection(tcpipClient)

    if commandClient:open() then -- '.sopas'-file is loaded
      -- Login to device
      local SetAccessModeNode = commandClient:createNode('SetAccessMode')
      SetAccessModeNode:set(4, 'NewMode') -- userlevel 'Service'
      SetAccessModeNode:set(0x81BE23AA, 'Password') -- encoded password
      local SetAccessModeSuccess,
        SetAccessModeResult =
        commandClient:invoke('SetAccessMode', SetAccessModeNode)
      assert(SetAccessModeSuccess)
      assert(SetAccessModeResult:get('success'), 'Login failed') -- failed to get access

      -- Set encoder setting to "no enocder"
      local LICencsetNode = commandClient:read('LICencset')
      local phaseSetting = LICencsetNode:getNode()
      phaseSetting:set(0) --0:"no enocder"
      LICencsetNode:setNode(phaseSetting)
      assert(
        commandClient:write('LICencset', LICencsetNode),
        'Setting encoder filter failed'
      )

      -- Run
      local RunSuccess,
        RunResult = commandClient:invoke('Run')
      assert(RunSuccess)
      assert(RunResult:get('success'), 'Run method not executed')

      -- Login to device
      SetAccessModeNode = commandClient:createNode('SetAccessMode')
      SetAccessModeNode:set(4, 'NewMode') -- userlevel 'Service'
      SetAccessModeNode:set(0x81BE23AA, 'Password') -- encoded password
      SetAccessModeSuccess,
        SetAccessModeResult =
        commandClient:invoke('SetAccessMode', SetAccessModeNode)
      assert(SetAccessModeSuccess)
      assert(SetAccessModeResult:get('success'), 'Login failed') -- failed to get access

      --Set encoder setting to "phase"
      LICencsetNode = commandClient:read('LICencset')
      phaseSetting = LICencsetNode:getNode()
      phaseSetting:set(2) --2:"phase"
      LICencsetNode:setNode(phaseSetting)
      assert(
        commandClient:write('LICencset', LICencsetNode),
        'Setting encoder filter failed'
      )

      -- Run
      RunSuccess,
        RunResult = commandClient:invoke('Run')
      assert(RunSuccess)
      assert(RunResult:get('success'), 'Run method not executed')
    end

    commandClient:close()
    tcpipClient:disconnect()
  end
end

--@readPowerOnCnt
local function readPowerOnCnt()
  local tcpipClient = TCPIPClient.create()
  tcpipClient:setIPAddress(scannerIP)
  tcpipClient:setPort(2112)
  tcpipClient:connect(5000)
  assert(tcpipClient:isConnected(), 'Failed to open a TCP/IP client')

  -- Create, configure and open the command client
  local commandClient = Command.Client.create()
  --the sopas file includes the variable structur and is therefor neeed
  commandClient:setDescriptionFile('resources/sopas/LMS41xxx.sopas')
  commandClient:setDeviceSelector('', 'LMS41xxx')
  commandClient:setProtocol('COLA_A')
  commandClient:setConnection(tcpipClient)

  local powerOnCnt
  if commandClient:open() then -- '.sopas'-file is loaded
    local PowerOnCntNode = commandClient:read('PowerOnCnt')
    powerOnCnt = PowerOnCntNode:get()
  end

  commandClient:close()
  tcpipClient:disconnect()

  return powerOnCnt
end

--@fillSICKcomponents()
local function fillSICKcomponents ()
Script.notifyEvent(
  'printLMS',
  'Device type\t\t' .. lidar.deviceType ..'\r\n' ..
  'Product number\t' .. lidar.orderNumber ..'\r\n' ..
  'Firmware\t\t' .. lidar.firmwareVersion ..'\r\n' ..
  'Serial number\t' .. lidar.serialNumber ..'\r\n' ..
  'IP adress\t\t' .. lidar.ipAdress)

Script.notifyEvent(
  'printSIM',
  'Device type\t\t' .. sim.deviceType ..'\r\n' ..
  'Product number\t' .. sim.orderNumber ..'\r\n' ..
  'Firmware\t\t' .. sim.firmwareVersion ..'\r\n' ..
  'Serial number\t' .. sim.serialNumber ..'\r\n' ..
  'IP adress\t\t' .. sim.ipAdress)

Script.notifyEvent(
  'printEncoder',
  'Device type\t\t' .. encoder.deviceType ..'\r\n' ..
  'Product number\t' .. encoder.orderNumber)

Script.notifyEvent(
  'printSwitches',
  'Device type\t\t' .. inductiveSwitch.deviceType ..'\r\n' ..
  'Product number\t' .. inductiveSwitch.orderNumber)
end

local function checkIP(ip)
    -- must pass in a string value
    if ip == nil or type(ip) ~= "string" then
        return false
    end

    -- check for format 1.11.111.111 for ipv4
    local chunks = {ip:match("(%d+)%.(%d+)%.(%d+)%.(%d+)")}
    if (#chunks == 4) then
        for _,v in pairs(chunks) do
            if (tonumber(v) < 0 or tonumber(v) > 255) then
                return false
            end
        end
        return true
    else
        return false
    end
end

--@drawLMS(angl)
local function drawLMS(scannerAngle)
  --yTrans: 20, move scanner out of center to match the data acquisition origin
  local transBlue =
    Transform.createRigidAxisAngle3D({0, 1, 0}, -math.rad(scannerAngle), 1, 20, 1)
  local lmsBoxBlue = Shape3D.createBox(129, 192, 78, transBlue) --scanner size
  scanViewer:addShape(lmsBoxBlue, lmsDecoBlue, 'lmsBoxBlue')

  --yTrans: 20, move scanner out of center to match the data acquisition origin
  --zTrans: -53, move black part down
  local transBlack =
    Transform.createRigidAxisAngle3D(
    {0, 1, 0},
    -math.rad(scannerAngle),
    1,
    20,
    -53,
    Point.create(0, 0, 53)
  )
  local lmsBoxBlack = Shape3D.createBox(129, 192, 28, transBlack) --scanner size
  scanViewer:addShape(lmsBoxBlack, lmsDecoBlack, 'lmsBoxBlack')

  --xTrans: 65, move laser picture to the right location
  --zTrans: -20, move laser picture to the right location
  local transRed =
    Transform.createRigidAxisAngle3D(
    {0, 1, 0},
    -math.rad(scannerAngle),
    65,
    1,
    -20,
    Point.create(-65, 0, 20)
  )
  local laser = Shape3D.createRectangle(2, 110, transRed) --scanner size
  scanViewer:addShape(laser, laserDeco, 'laser')
end

--@updateThumbnailCounter()
local function updateThumbnailCounter()
  if numberOfStoredFiles == 0 then
    Script.notifyEvent('updateFileCount', '0/0')
  else
    Script.notifyEvent('updateFileCount', thumbnailIndex ..'/'..numberOfStoredFiles)
  end
end

--@printThumbnail()
local function printThumbnail()
  thumbnailViewer:clear()
  if numberOfStoredFiles > 0 and pointClouds[thumbnailIndex].pcd ~= 0 then
    thumbnailViewer:addPointCloud(pointClouds[thumbnailIndex].pcd, pointCloudDeco)
    Script.notifyEvent('printThumbnailFileName', pointClouds[thumbnailIndex].filename)
    Script.notifyEvent('printThumbnailCharacteristics',
    'Points:\t\t' .. tostring(pointClouds[thumbnailIndex].points) .. '\r\n' ..
    'Size:\t\t' ..   pointClouds[thumbnailIndex].size .. ' MB' ..    '\r\n' ..
    'Width (X):\t' ..  pointClouds[thumbnailIndex].width  ..' mm' .. '\r\n' ..
    'Length (Y):\t' .. pointClouds[thumbnailIndex].length ..' mm' .. '\r\n' ..
    'Height (Z):\t' .. pointClouds[thumbnailIndex].height ..' mm')
    
    Script.notifyEvent('setDownloadFilePath', '/'.. archivePath .. pointClouds[thumbnailIndex].filename)
    Script.notifyEvent('setDownloadFileName', pointClouds[thumbnailIndex].filename)
    Script.notifyEvent('deactivateDeleteAllFiles', false)

    -- do not enable the button if we already present this one
    if appState == 'PointCloud saved!' and thumbnailIndex == 1 then
      Script.notifyEvent('deactivateLoadPointCloud', true)
    elseif appState == 'Presenting the PointCloud' and thumbnailIndex == 1 then
      Script.notifyEvent('deactivateLoadPointCloud', true)
    else
      Script.notifyEvent('deactivateLoadPointCloud', false)
    end

  else
    Script.notifyEvent('printThumbnailFileName', '')
    Script.notifyEvent('printThumbnailPoints', '')
    Script.notifyEvent('printThumbnailLength', '')
    Script.notifyEvent('printThumbnailWidth', '')
    Script.notifyEvent('printThumbnailHeight', '')
    Script.notifyEvent('printThumbnailMemSize', '')
    Script.notifyEvent('setDownloadFilePath', '')
    Script.notifyEvent('setDownloadFileName', '')
    Script.notifyEvent('deactivateDeleteAllFiles', true)
    Script.notifyEvent('deactivateLoadPointCloud', true)
  end

  updateThumbnailCounter()
  thumbnailViewer:present()
end

--@handleEncoderOfstIfUpperPos()
local function handleEncoderOfstIfUpperPos() --asume encoder value is 0 and LMS is 90° vertically
    if PortDI1:get() then -- check upper end position switch
      encoderIncrementsOffset = -603 --asume encoder value is 0 and LMS is 90° vertically; ~90° --> 603 increments
    end
end

--@readPointCloudsFromArchive()
local function readPointCloudsFromArchive()
  if File.isdir(archivePath) == false then
    File.mkdir(archivePath)
    File.copy(sampleSource, archivePath .. "/sample.pcd")
    numberOfStoredFiles = 0
  end

  local recordList = File.list(archivePath)
  numberOfStoredFiles = #recordList
  local head = 1
  for i = head, #recordList do  --delete all PointClouds in RAM
    table.insert(
      pointClouds,
      head,
      {
        pcd = 0,
        filename = '',
        points = '',
        length = '',
        width = '',
        hight = '',
        size = ''
      }
    )
    pointClouds[head].pcd = PointCloud.load(archivePath .. recordList[(#recordList-i)+1])
    pointClouds[head].filename = recordList[(#recordList-i)+1]
    local numberOfPoints, w, h = pointClouds[head].pcd:getSize()
    pointClouds[head].points = numberOfPoints
    local boundingBox = pointClouds[head].pcd:getBoundingBox()
    local xmin, ymin, zmin, xmax, ymax, zmax = boundingBox:getBounds()
    pointClouds[head].length = math.floor((ymax - ymin)*100)/100 --truncate
    pointClouds[head].width =  math.floor((xmax - xmin)*100)/100 --truncate
    pointClouds[head].height = math.floor((zmax - zmin)*100)/100 --truncate
    --estimates needed memory (MB) for cloud and truncate
    pointClouds[head].size = math.floor((numberOfPoints* 0.0000267)*100)/100
  end
end

------------------------------------------------------------------------------------------------------------------------
-- handled events
------------------------------------------------------------------------------------------------------------------------

--@handleOnConnect()
local function handleOnConnect()
  if appState == 'Presenting the PointCloud' then
    scanViewer:clear()
    drawLMS(angle)
    scanViewer:addPointCloud(pointClouds[thumbnailIndex], pointCloudDeco)
    scanViewer:present()
  end

  fillSICKcomponents()
  Script.notifyEvent('updateState', appState)

  if scanprovider ~= nil then
    Script.notifyEvent('deactivateReconnect', true)  -- deactivating button as live data is already present
  end
  Script.notifyEvent('deactivateReturnLiveData', true)  -- deactivating button as live data is already present

  printThumbnail()
end
scanViewer:register('OnConnect', handleOnConnect)

--@timerExp()
local function timerExp()
  count100ms = count100ms + 1
  if appState == 'Collecting data ...' then
    timer1:start()
    if count100ms > recTime / 0.1 then --stop recording if rec time is reached
      count100ms = 0
      timer1:stop()
      appState = 'PointCloud saved!'
    end
  end
end
timer1:register('OnExpired', timerExp)

--@loadPointCloud():
local function loadPointCloud()
  if pointClouds[thumbnailIndex].pcd ~= 0 then
    scanprovider:stop()
    scanViewer:clear()
    scanViewer:addPointCloud(pointClouds[thumbnailIndex].pcd, pointCloudDeco)
    scanViewer:present()
    Script.notifyEvent('deactivateReturnLiveData', false)
  end
end
Script.serveFunction("DemoCase_LMS4000.loadPointCloud", loadPointCloud)

--@returnLiveData():
local function returnLiveData()
  scanprovider:start()
  appState = 'Reset recording'
  Script.notifyEvent('deactivateReturnLiveData', true)
end
Script.serveFunction("DemoCase_LMS4000.returnLiveData", returnLiveData)

--@deleteCurrentPCD():
local function deleteCurrentPCD()
  local recordList = File.list(archivePath)
  if #recordList > 0 then
    --print(archivePath..pointClouds[thumbnailIndex].filename)
    File.del(archivePath..pointClouds[thumbnailIndex].filename)
    table.remove(pointClouds, thumbnailIndex)
    thumbnailIndex = thumbnailIndex - 1
    if thumbnailIndex == 0 then
      thumbnailIndex = 1
    end
    
    numberOfStoredFiles = numberOfStoredFiles -1
  end

  if appState == 'Presenting the PointCloud' then
    appState = 'Reset recording'
  end

  printThumbnail()
end
Script.serveFunction("DemoCase_LMS4000.deleteCurrentPCD", deleteCurrentPCD)

--@deleteAllFiles():
local function deleteAllFiles()
  File.del(archivePath) --delete all PointClouds at SD card
  File.mkdir(archivePath)

  for i = 1, numberOfStoredFiles do  --delete all PointClouds in RAM
    pointClouds[i].pcd = 0
    pointClouds[i].filename = ''
    pointClouds[i].points = ''
    pointClouds[i].length = ''
    pointClouds[i].width = ''
    pointClouds[i].hight = ''
    pointClouds[i].size = ''
  end

  File.copy(sampleSource, archivePath .. "sample.pcd")
  readPointCloudsFromArchive() -- read in the sample PCD

  thumbnailIndex = 1
  local recordList = File.list(archivePath)
  numberOfStoredFiles=#recordList

  if appState == 'Presenting the PointCloud' then
    appState = 'Reset recording'
  end

  printThumbnail() -- as all deleted, all thumbnails cleared
end
Script.serveFunction("DemoCase_LMS4000.deleteAllFiles", deleteAllFiles)

--@previousPointCloud():
local function previousPointCloud()
 thumbnailIndex = thumbnailIndex - 1
 if thumbnailIndex < 1 then
   thumbnailIndex = 1
 end
 printThumbnail()
end
Script.serveFunction("DemoCase_LMS4000.previousPointCloud", previousPointCloud)

--@nextPointCloud():
local function nextPointCloud()
 thumbnailIndex = thumbnailIndex + 1
 if thumbnailIndex > numberOfStoredFiles then
   thumbnailIndex = numberOfStoredFiles
 end

 if thumbnailIndex < 1 then
   thumbnailIndex = 1
 end
 printThumbnail()
end
Script.serveFunction("DemoCase_LMS4000.nextPointCloud", nextPointCloud)

--@reconnectLiDAR():
local function reconnectLiDAR()
  init()
end
Script.serveFunction("DemoCase_LMS4000.reconnectLiDAR", reconnectLiDAR)

--@IPadressInput(input:transBlue
local function IPadressInput(input)
  if checkIP(input) == true then
    scannerIP = input
    lidar.ipAdress = input
    Script.notifyEvent('deactivateApplySettings', false)
  else
    Script.notifyEvent('deactivateApplySettings', true)
  end
end
Script.serveFunction("DemoCase_LMS4000.IPadressInput", IPadressInput)

--@meanFilterSizeInput(input:int):
local function meanFilterSizeInput(input)
  if meanFilterSize ~= input then
    Script.notifyEvent('deactivateApplySettings', false)
    meanFilterSize = input
  else
    Script.notifyEvent('deactivateApplySettings', true)
  end
end
Script.serveFunction("DemoCase_LMS4000.meanFilterSizeInput", meanFilterSizeInput)

--@recTimeInput(input:int):
local function recTimeInput(input)
  if recTime ~= input then
    Script.notifyEvent('deactivateApplySettings', false)
    recTimeBuf = input
  else
    Script.notifyEvent('deactivateApplySettings', true)
  end
end
Script.serveFunction("DemoCase_LMS4000.recTimeInput", recTimeInput)

--@applySettings():
local function applySettings()
  init()
  recTime = recTimeBuf
  Script.notifyEvent('deactivateApplySettings', true)
end
Script.serveFunction("DemoCase_LMS4000.applySettings", applySettings)

--@endPosBot(state)
local function endPosBot(state) -- Function is called when Input 2 changes, lower end switch
  encoderIncrementsOffset = encoderIncrements -- set encoder Offset
  if state == true then
    if appState == 'Presenting the PointCloud' then
      appState = 'Reset recording'
    end
  elseif state == false then
    if appState == 'Move the LiDAR to the upper position to record data.' then --collect data
      appState = 'Collecting data ...'
      timer1:start() -- start timer to not run in memory overflow, set by <recTime>
    end
  end
end
PortDI2:register('OnChange', endPosBot)

--@endPosTop(state)
local function endPosTop(state) -- Function is called when Input 1 changes
  if state == true then --if end postion reached
    if
      appState ~= 'Move the LiDAR to the lower position to enable the recording.' and
        appState ~= 'PointCloud saved!' and
        appState ~= 'Presenting the PointCloud'
     then
      appState = 'PointCloud saved!'
      timer1:stop()
    end
  elseif state == false then --if end postion left
    if
      appState ~= 'Move the LiDAR to the lower position to enable the recording.' and
        appState ~= 'Presenting the PointCloud'
     then
      appState = appState --do nothing; --appState = 'Collecting data ...' not needed anymore
    elseif appState == 'Presenting the PointCloud' then
      appState = 'Reset recording'
    end
  end
end
PortDI1:register('OnChange', endPosTop)

--@handleOnNewScan
local function handleOnNewScan(scan, sensordata)
  local rssiProfile = Scan.toProfile(scan, "RSSI")
  rssiProfile = rssiProfile:multiplyConstant(10000)
  Scan.importFromProfile(scan, rssiProfile, "RSSI") --replace old rssi values with new ones

  scanCount = scanCount + 1 --count scans
  local sdstr = sensordata:toString() -- convert to string
  local positionEncoderInString
  for sstr in string.gmatch(sdstr, '([^,]+)') do --look for position of encoder value
    positionEncoderInString = string.find(sstr, 'LocalIncrement')
    if positionEncoderInString then
      encoderIncrements =
        tonumber(string.sub(sstr, positionEncoderInString + 14)) -- if valid then convert to int
      break
    end
  end

  if encoderIncrements > (2 ^ 32) / 2 then --handle overflow
    encoderIncrements = encoderIncrements - 2 ^ 32
  end

  angle = (encoderIncrements - encoderIncrementsOffset) * 0.144 --correct offset and calc angle in degree
  trafo:setYawPitchRoll(0, -math.rad(angle), 0)
  local cloud = trafo:transformToPointCloud(scan)

  if scanCount % 10 == 0 then --only show every 10th scan, reduce load
    --print('[DEBUG]: app state: ' .. appState.. '; scan: ' .. scanCount .. '; angle: ' .. angle) -- for debugging

    if appState == 'Collecting data ...' then
      Script.notifyEvent('updateState', appState..' ['.. (count100ms/10) .. 's/' .. recTime .. 's]')
    elseif appState ~= appStateOld then --update only if new data
      Script.notifyEvent('updateState', appState)
      appStateOld = appState
    end

    if numberOfStoredFiles ~= numberOfStoredFilesOld or
    thumbnailIndex ~= thumbnailIndexOld then  --update only if new data
      updateThumbnailCounter()
      numberOfStoredFilesOld = numberOfStoredFiles
      thumbnailIndexOld = thumbnailIndex
    end

    if appState ~= 'Presenting the PointCloud' then
      scanViewer:clear()
      drawLMS(angle)
      scanViewer:addPointCloud(cloud, pointCloudDeco)
      scanViewer:present()
    end
  end

  -- statemachine (follows this order); states are changed by timers, buttons and inductive switches
    --'LiDAR not connected. Check wiring and reconnect.'
    --'Move the LiDAR to the lower position to enable the recording.'
    --'Move the LiDAR to the upper position to record data.'
    --'Collecting data ...'
    --'PointCloud saved!'
    --'Presenting the PointCloud'
    --'Reset recording'
    --(start again with 1st step)

  if appState == 'Move the LiDAR to the lower position to enable the recording.' then
    if PortDI2:get() then -- check lower end position switch
      encoderIncrementsOffset = encoderIncrements --set a virtual end position until device is moved
      appState = 'Move the LiDAR to the upper position to record data.'
    end
  elseif appState == 'Move the LiDAR to the upper position to record data.' then
    appState = appState --do nothing
  elseif appState == 'Collecting data ...' then
    pointCloudCollector:collect(cloud, true)
  elseif appState == 'PointCloud saved!' then
    scanprovider:stop()
    local head = 1
    table.insert(
      pointClouds,
      head,
      {
        pcd = 0,
        filename = '',
        points = '',
        length = '',
        width = '',
        hight = '',
        size = ''
      }
    )
    pointClouds[head].pcd = pointCloudCollector:collect(cloud, false)
    pointClouds[head].filename =
      string.format('%05d', lidar.powerOnCounter) ..
      string.format('%07d', math.floor(DateTime.getTimestamp()/1000)) .. '.pcd' -- short timestamp to seconds

    scanViewer:clear()
    drawLMS(angle)
    scanViewer:addPointCloud(pointClouds[head].pcd, pointCloudDeco)
    scanViewer:present()

    -- add items to the point cloud like points length, width, height, size
    local numberOfPoints, w, h = pointClouds[head].pcd:getSize()
    pointClouds[head].points = numberOfPoints
    if pointClouds[head].points > 0 then --if LMS4000 is covered (optical path blocked) no data points are provided
      local boundingBox = pointClouds[head].pcd:getBoundingBox()
      local xmin, ymin, zmin, xmax, ymax, zmax = boundingBox:getBounds()
      pointClouds[head].length = math.floor((ymax - ymin)*100)/100 --truncate
      pointClouds[head].width =  math.floor((xmax - xmin)*100)/100 --truncate
      pointClouds[head].height = math.floor((zmax - zmin)*100)/100 --truncate
      --estimates needed memory (MB) for cloud and truncate
      pointClouds[head].size = math.floor((numberOfPoints* 0.0000267)*100)/100
      local recordList = File.list(archivePath)
      local remainingFileSpace = File.getDiskFree(archivePath) / 1000000 -- in MB

      while (remainingFileSpace-10) < pointClouds[head].size do  -- reserve 10 MB of memory
        print(
          '[INFO]: remaining file space: ' .. remainingFileSpace ..
          '; cloud memory estimation: ' .. pointClouds[head].size
        )
        File.del(archivePath.. recordList[1]) -- delete the first (oldest) one
        table.remove(pointClouds, #recordList) --remove last item from table; oldest one
        remainingFileSpace = File.getDiskFree(archivePath)
      end

      pointClouds[head].pcd:save(archivePath..pointClouds[head].filename)
      ledResult:activate()
      Script.sleep(200) -- blink result LED
      ledResult:deactivate()

      recordList = File.list(archivePath)
      numberOfStoredFiles=#recordList
      thumbnailIndex = head
      printThumbnail()
    else
      --remove thos PointCloud from RAM because PointCloud is empty
      table.remove(pointClouds, 1)
    end
    scanprovider:start()
    appState = 'Presenting the PointCloud'
  elseif appState == 'Presenting the PointCloud' then
    appState = appState --do nothing
  elseif appState == 'Reset recording' then
    count100ms = 0
    appState = 'Move the LiDAR to the lower position to enable the recording.'
  else
    print('[ERROR]: implausible state')
  end
end

-------------------------------------------------------
-- main
------------------------------------------------------
local function main()
  
  scanQueueHandle = Script.Queue.create()
  scanQueueHandle:setMaxQueueSize(50)
  scanQueueHandle:setFunction(handleOnNewScan)

  print ("[INFO]: read PointClouds from file system")
  if File.exists (archivePath) == false then
    File.mkdir(archivePath)
  end
  readPointCloudsFromArchive()
  printThumbnail()
  Script.notifyEvent('deactivateReturnLiveData', true) -- deactivating button as live data is already present
  Script.notifyEvent('deactivateApplySettings', true)
  Script.sleep(10000) --wait for 10s to power up the LMS4000 first.
  init()
end
Script.register('Engine.OnStarted', main)

function init ()
  print ("[INFO]: setup LMS4000")
  if setupLMS() == false then
    Script.notifyEvent('deactivateReconnect', false)
    appState = 'LiDAR not connected. Check wiring and reconnect.'
    Script.notifyEvent('updateState',appState)
  else
    Script.notifyEvent('deactivateReconnect', true)
    fillSICKcomponents()
    resetEncoderValue()
    lidar.powerOnCounter = readPowerOnCnt()
    handleEncoderOfstIfUpperPos()
    PowerHandleS1:enable(true) -- power induktive end switches
    scanprovider = Scan.Provider.RemoteScanner.create()
    if scanprovider ~= nil then
      scanprovider:setIPAddress(scannerIP)
      scanprovider:setSensorType('LMS4XXX')
      scanprovider:register('OnNewScan', handleOnNewScan)
      scanprovider:start()
      ledSysReady:setColor({0,255,0})
      ledSysReady:activate()
    else
      ledSysReady:setColor({255,0,0})
      ledSysReady:activate()
    end
  end
end